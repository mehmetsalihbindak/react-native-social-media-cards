Easily prepare awesome social media cards for your React Native app. 


## Install

```
$ npm install react-native-social-media-cards 

$ react-native link react-native-ionicons 
```

## Usage

```js
import {SocialMedia} from 'react-native-social-media-cards'

...

<SocialMedia
    text="Youtube"
    textColor='#ffffff'
    logo="logo-youtube"
    link="https://youtube.com/"
    backgroundColor= '#ff0000' 
    size={35}
    logoColor='#ff0000'
/>

```

# ScreenShots
![Example of Social Media Cards in Android](https://gitlab.com/mehmetsalihbindak/react-native-social-media-cards/raw/master/ss-android.png) 



![Example of Social Media Cards in iOS](https://gitlab.com/mehmetsalihbindak/react-native-social-media-cards/raw/master/ss-ios.png)


## Properties

 name                  | description                                 | type     
:--------------------- |:------------------------------------------- | --------
 text                  | Your nickname or name of social media account   |   String (OPTIONAL)
 textColor             | Color property of the text            |   String (OPTIONAL)
 logo                  | Choose a logo from ionicons designer pack          |   String (OPTIONAL)
 link                  | https link of social media account          |   String (OPTIONAL)
 backgroundColor       | Card's background color          |   String (OPTIONAL)
 size                  | size of logo          |   Number (OPTIONAL)
 logoColor             | color of logo         |   String (OPTIONAL)

## Examples

Look examples.js file for using examples. 

## Special Note 
This is my first NPM Package experience. If there is any mistakes, please feel free to say it.

