import {  StyleSheet} from 'react-native';

export default StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'

    },
    iconWrapper:{
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:60,
        height:60,
        backgroundColor:'#fff',
        borderRadius:120,
        marginLeft:20
    },
    text:{
        paddingRight: 20,
        fontSize:50
    }
})