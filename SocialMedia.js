import PropTypes from 'prop-types';
import React from 'react';
import { View, Text, TouchableOpacity, Linking } from 'react-native';

import styles from './styles';

import { sanFranciscoWeights } from 'react-native-typography'
import Icon from 'react-native-ionicons';

const SocialMedia = ({text, textColor, logo, logoColor, link, backgroundColor, size}) => (

    <View style={[{backgroundColor:backgroundColor},styles.container]}>
            <TouchableOpacity
                onPress={()  => Linking.openURL(link)
                .catch( () => alert("There is an error, Please try again later") )}
                style={[styles.iconWrapper]}>
                <Icon name={logo} style={{color:logoColor,fontSize:size}}/>
            </TouchableOpacity>
            <Text 
                style = {[sanFranciscoWeights.ultraLight, 
                        {color:textColor},
                        styles.text ]}>{text}
            </Text>
        </View>

);

SocialMedia.propTypes = {
    text: PropTypes.string,
    textColor: PropTypes.string,
    logo: PropTypes.string,
    link: PropTypes.string,
    backgroundColor:PropTypes.string,
    logoColor: PropTypes.string,
    size: PropTypes.number
}

SocialMedia.defaultProps = {
    text: 'awesome',
    textColor: '#fff',
    logo: 'ios-infinite',
    link: 'https://github.com',
    backgroundColor: '#000000',
    logoColor: '#000000',
    size: 50
}

export default SocialMedia;

