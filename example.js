import React, {Component} from 'react';
import {StyleSheet,View} from 'react-native';
import {SocialMedia} from 'react-native-social-media-cards'

export default class Example extends Component{
  render() {
    return (
      <View style={styles.container}>
        <SocialMedia backgroundColor='red' logoColor='red'  />
        <SocialMedia backgroundColor='yellow' textColor='black' logo='ios-play' />
        <SocialMedia
          text= 'awesome'
          textColor= '#fff'
          logo= 'ios-leaf'
          link= 'https://github.com'
          backgroundColor= '#aa0000'
          logoColor= '#000000'
          size={35}
        />
        <SocialMedia/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
